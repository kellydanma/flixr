# Rails

Some notes about directory structure, since this is my first time experimenting with Ruby and Rails.

Notes are not my own; they are from [Pragmatic Studio](https://online.pragmaticstudio.com/).

## app

`app` is where you'll put the bulk of your application code. This is where you'll spend most of your time. Rails puts a lot of emphasis on keeping code organized, so the app directory has a number of subdirectories:

### models, views, and controllers

`models`, `views`, and `controllers` are appropriately-named directories where you'll organize your (wait for it) model, view, and controller code.

### assets

`assets` contains sub-directories where you'll store your application's static assets such as images and stylesheets.

### channels

`channels` is where you put Ruby classes for handling real-time features using Action Cable.

### helpers

`helpers` is where you put Ruby modules that define utility methods for views.

### javascript

`javascript` is where you put JavaScript modules that get compiled by Webpack.

### jobs

`jobs` is where you put Ruby classes for running background jobs.

### mailers

`mailers` is where you put Ruby classes for generating and sending e-mails.

## bin

`bin` isn't something you'll mess with. It contains the rails script file and other scripts.

## config

`config` is where you go to tweak the configuration settings of your application. Initially you won't have to touch most of the files in here because generated Rails apps are configured with "sensible defaults". Later in this course we'll make stops at the following key places:

### database.yml

`database.yml` configures the database used by each environment.

### routes.rb

`routes.rb` maps incoming requests (URLs) to application code.

### environments

`environments` is a directory that contains three files that define specific settings for each environment: development, test, and production.

### initializers

`initializers` is a directory where you put Ruby code that needs to be run when the application starts.

## db

`db` contains everything related to the database, including migration files and, in the case of using the default SQLite3 database, the database file itself.

## lib

`lib` is where you put any reusable "library" code that's not a model, view, or controller. It's more common these days to package this sort of code as a gem. But it has one important subdirectory:

### tasks

`tasks` is where you would put any custom Rake tasks for your application, with each file having a .rake extension.

## log

`log` is where Rails automatically creates log files so you have a record of what happened when your app ran. Each environment gets its own log file.

## public

`public` is the document "root" directory of the app. It contains static files that are served directly by the web server. For example, 404.html lives here and gets served when a page can't be found.

## storage

`storage` is where Active Storage stores uploaded files when running in the development environment.

## test

`test` is where you put test files if you use the default testing library.

## tmp

`tmp` is where Rails stores any temporary files needed by the app. We can't recall the last time we even peeked in this directory.

## vendor

`vendor` is where, in the old days, we used to put third-party code. Nowadays we generally use gems declared in the Gemfile instead.

## Gemfile

Last, but by no means least, the `Gemfile` file in the top-level directory contains a list of third-party dependencies (Ruby gems) that your application needs.
