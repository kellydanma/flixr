# flixr

## About

Flixr hosts a repository of movies; currently supported operations include creating, updating, and deleting a film.

![](./lib/assets/homepage.PNG)

![](./lib/assets/one_film.PNG)

## Requirements

- Ruby 2.6
- Rails 6.0

## Getting started

Clone the repository, then:

```bash
cd flixr

# bundle install required gems
bundle install --without production

# run Rails migration to create database tables from schema
rails db:migrate

# seed the database
rails db:seed

# start the server
rails s
```

The project is hosted at `localhost:3000`.

To view routing, see `localhost:3000/rails/info/routes`.

### Acknoledgements

Tutorial from [Pragmatic Studio](https://pragmaticstudio.com/rails).
