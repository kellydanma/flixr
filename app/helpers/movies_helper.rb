module MoviesHelper

  # if a movie grosses less than $225M, it's a flop
  def total_gross(movie)
    if movie.flop?
      "Flop!"
    else
      number_to_currency(movie.total_gross, precision: 0)
    end
  end

  # display first 140 characters of movie description
  def description(movie)
    truncate(movie.description, length: 150, separator: ' ')
  end

  # renders release date in a human-friendly way
  def release_date(movie)
    movie.released_on.strftime("Released %B %-d, %Y")
  end
end
