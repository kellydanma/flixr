class MoviesController < ApplicationController
  def index
    @movies = Movie.released # only display released movies
    # @movies = Movie.all
  end

  # displays one film in a separate page using primary key
  def show
    @movie = Movie.find(params[:id])
  end

  # edit one film as admin
  def edit
    @movie = Movie.find(params[:id])
  end

  # patch a movie
  def update
    @movie = Movie.find(params[:id])
    if @movie.update(movie_params)
      redirect_to @movie, notice: "Movie successfully updated!"
    else
      render :edit
    end
  end

  # add a new film page
  def new
    @movie = Movie.new
  end

  # create new film
  def create
    @movie = Movie.new(movie_params)
    if @movie.save
      redirect_to @movie, notice: "Movie successfully added!"
    else
      render :new
    end
  end

  # delete a film
  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    redirect_to movies_url, alert: "Movie successfully deleted!" # redirect to /movies
  end

  private

  private

    # allow manipulation of all fields for now
    def movie_params
      params.require(:movie).
        permit(:title, :description, :rating, :released_on,
          :total_gross, :director, :duration, :image_file_name)
    end
end
